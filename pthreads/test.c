#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

/*
 * Det här programmet skapar en ny tråd som försöker läsa data arrayen som skapas på rad 28. 
 * Tråden som skapas läser bara data så länge en viss mutex, den skapas på rad 39 inte är låst.
 * OM den är låst så yieldar tråden.
 */

// Prototypes
void *greeter(void *);

// This struct contains everything a new thread
// need to know.
struct pu_thread_container {
				unsigned short *status_vector;
				size_t vector_length;
				pthread_mutex_t *mutex_handle;
};


int main()
{
				printf("main(): Main method\n");

				// Test data status vector.
				unsigned short data[10];
				int i;
				for(i = 0; i < 10; i++)
								data[i] = 0;

				// POSIX Thread handle.
				pthread_t thread_handle;

				// Mutex handle
				pthread_mutex_t mutex_handle;

				// Initialize the mutex with the default settings.
				pthread_mutex_init(&mutex_handle, NULL);

				// lock the mutex
				pthread_mutex_lock(&mutex_handle);

				// Create and set the pu_thr_cont.
				struct pu_thread_container pu_thr_info;
				pu_thr_info.status_vector = data;
				pu_thr_info.vector_length = sizeof(data) / sizeof(short);
				pu_thr_info.mutex_handle = &mutex_handle;

				// Create a thread using default settings.
				pthread_create(&thread_handle, NULL, greeter, &pu_thr_info);

				// Yield immediately. This will cause greeter to be blocked by the mutex!
				printf("sleeping from main.\n");

				// Sleep for one second. This will cause greeter to be blocked.
				sleep(1);

				printf("Unlocking status vector mutex.\n");
				pthread_mutex_unlock(&mutex_handle);

				// wait for the thread to finish
				pthread_join(thread_handle, NULL);

				// delete mutex
				pthread_mutex_destroy(&mutex_handle);
}

/*
 * Simple method that says hello 
 */
void *greeter(void *arg) 
{
				int i = 0;
				struct pu_thread_container pu_thr_cont;
				pu_thr_cont = *(struct pu_thread_container*)arg;

				printf("Printing pu_thread_container data from thread:\n");
				printf("pu_thr_cont.vector_length: %i\n", pu_thr_cont.vector_length);
				printf("pu_thr_cont.status_vector: ");
				for(i = 0; i < pu_thr_cont.vector_length; i++)
								printf("%i ", pu_thr_cont.status_vector[i]);
				printf("\ndone printing data.\n");

				printf("Trying to lock/read from the status vector.\n");
				while (pthread_mutex_trylock(pu_thr_cont.mutex_handle) == EBUSY)
				{
						printf("Tried to read but it was busy.\n yielding...\n");
						sched_yield();
				}
				printf("Read from status_vector. Value was: %i\n", pu_thr_cont.status_vector[0]);
}
