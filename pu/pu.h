#ifndef _PU_H_
#define _PU_H_
/* 
 *  This file contains everything that has to do with the PU protocol.
 *  Inclide this file in your program to use PU.
 *
 *  Written by Kim Nilsson, Otto Nordgren, 2012.
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

// Debug flag. Enables some extra output.
#define PU_DEBUG 0
// Maximum packet size
#define PU_MAX_PACKET_SIZE 512
// Maximum retransmissions before failure
#define PU_MAX_RETRANSMISSIONS 3
// Time to wait before retransmit
#define PU_RETRANSMISSION_TIME 1
// Packat has arrived flag
#define PU_ACK -2
// Packet has been sent, not arrived
#define PU_SENT 0

// Struct that holds information about each packet to be sent.
struct pu_packet {
				int packet_id;
				int total_size;
				char *data;
};

// This struct contains information needed by the retransmission function.
struct pu_resend_handler_info {
				int sock;
				short *status_vector;
				int *status_vector_ids;
				int status_vector_length;
				struct pu_packet *packet_info;
				struct sockaddr *receiver;
				pthread_mutex_t *ack_mutex_handle;// Pointer to status_vector mutex.
};

// This struct contains information about a PU socket.
struct pu_info {
				int sock; 											// An identifier to the UDP socket.
				struct hostent *host;	  				// DNS response about server.
				struct sockaddr_in server_addr; // Address information to the server.
};

// This struct contains everything the ACK-handler needs to know.
struct pu_ack_handler_info {
				int sock; 												// Socket id
				short *status_vector; 						// Pointer to status vector.
				int *status_vector_ids;						// IDs
				int status_vector_length;					// Size of status vector.
				pthread_mutex_t *ack_mutex_handle;// Pointer to status_vector mutex.
				struct sockaddr* receiver;				// Address to read from
};

// Function prototypes
int pu_init(struct pu_info *pu_handle,const char *address,const int port);
int pu_destroy(const struct pu_info *pu_handle);
int pu_send(const struct pu_info *pu_handle, const char * const data, size_t len);
void *ack_handler(void *arg);
void *resend_handler(void *arg);
void dp(const char *format, ...);

#endif
