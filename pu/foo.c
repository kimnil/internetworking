#include <stdio.h>

int main(int args, int argv[]) {
  char ch;
  char buffer[80];
  int chars_read = 0;

  while((chars_read < 80) && (ch = fgetc(stdin)) && ch != '\n') {
    buffer[chars_read] = ch;
  
    chars_read++;
  }

  if(chars_read < 80) {
    buffer[chars_read] = '\0';
  }

  printf("%s och %d \n", buffer, chars_read);

  return 0;
}
