#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "pu.h"

#define PORT_NUM 6666

//prototypes
void timer_start();
void timer_stop();

// for the timer
int var_timer;

int main(int argc, char *argv[])
{
		struct pu_info pu_handle; 			// Holds info about pu socket.
		char *bigtext;
		char *tinytext;
    char ch;
    char buffer[80];
    int chars_read = 0;
    int file_size = 0;
    FILE *fp;

    fp = fopen("data.txt", "r");

    while((ch = fgetc(fp)) != EOF) {
      file_size++;
    }
		rewind(fp);

    char *file_vector;

    file_vector = (char *)malloc(file_size+1);

    for(int i = 0; (ch = fgetc(fp)) != EOF; i++) {
      file_vector[i] = ch;
    }

    file_vector[file_size] = '\0';

		// Allocate memory for text to send.
		bigtext  = (char *)malloc(sizeof(char) * 128000);
		tinytext = (char *)malloc(sizeof(char) * 64);

		// fill texts
		int i;
		for(i = 0; i < 64-1; i++)
						tinytext[i] = 't';

		for(i = 0; i < 128000-1; i++)
						bigtext[i] = 'b';

		tinytext[0] = 'a';
		tinytext[64-1] = 'b';
		bigtext[128000-1] = 'c';
		//bigtext[128000-1] = tinytext[64-1] = '\0';

		if (argc != 2)
		{
						printf("Usage ./test <address>\n");
						exit(0);
		}

		if(PU_DEBUG) 
		{
						printf("tinytext: %s\n", tinytext);
						printf("size of tinytext: %i\n", (int)sizeof(tinytext));
						//printf("bigtext: %s\n", bigtext);
		}

		pu_init(&pu_handle, argv[1], PORT_NUM); // set up socket
		//pu_send(&pu_handle, tinytext, sizeof(char)*64); // send stuff
		//pu_send(&pu_handle, bigtext, sizeof(char)*128000); // send stuff
		int k = 0;
		while (1) {
				pu_send(&pu_handle, file_vector, sizeof(char)*file_size+1); // send stuff
				printf("Done sending for the %ith time!\n", ++k);
				break;
		}
		printf("send complete");
		// WARNING
		return;

print:    
    while((chars_read < 80) && (ch = fgetc(stdin)) && ch != '\n') {
      buffer[chars_read] = ch;
    
      chars_read++;
    }

    if(chars_read < 80) {
      buffer[chars_read] = '\0';
    }

    if((chars_read == 1) && (buffer[0] = 'q')) {
      goto end;
    }
    else {
      pu_send(&pu_handle, buffer, sizeof(char)*chars_read);
      goto print;
    }

    printf("%s och %d \n", buffer, chars_read);

end:
		pu_destroy(&pu_handle); // tear down socket

		// Restore memory
		free((char *)bigtext);
		free((char *)tinytext);
		
		return 0;
}

void timer_start()
{
				var_timer = clock();
}	

void timer_stop()
{
				int elapsed = ((clock()-var_timer) / CLOCKS_PER_SEC);
				printf("%i seconds passed.\n", elapsed);
}
