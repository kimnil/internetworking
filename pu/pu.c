/* 
 *  This file contains everything that has to do with the PU protocol.
 *  Inclide this file in your program to use PU.
 *
 *  Written by Kim Nilsson, Otto Nordgren, 2012.
 */
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <pthread.h>
#include "pu.h"

/**
 * Simple wrapper around printf for debug output.
 */
void dp(const char *format, ...) 
{
				if(!PU_DEBUG)
								return;

				va_list args;
				va_start(args, format);
				vfprintf(stdout, format, args );
				fflush(stdout);
				va_end( args );
}

/* 
 * Clean up a PU socket.
 *
 * Params: info - The socket to destroy.
 * Return: 1 if everything went well, -1 otherwise.
 */
int pu_destroy(const struct pu_info *info) 
{
				if(close(info->sock) != 0)
				{
								printf("Fatal error! Could not close socket!\n");
								return -1;
								// Här kan man skriva ut mer information av vad som
								// faktiskt gick snett!
				}
				else
				{
								dp("Socket successfully closed.\n");
				}
				return 1;
}

/*
 * Initiliaze PU socket.
 *
 * Params: info - A pointer to a pu_info struct containing info about
 * 				 the host to open a socket to.
 * 				 addr - A char array containing the address to the server.
 * 				 port - The port number to connect to. Note: little-endian!
 * Return: 1 if everything went well, -1 otherwise.
 * 				 Note: No error codes are returned but are printed to stdout.
 */
int pu_init(struct pu_info *info,const char *addr,const int port) 
{
				char pip[256]; // Presentable IP.
				pip[255] = '\0'; // This might not be needed, but hey!

				// DNS lookup.
				info->host = (struct hostent *)gethostbyname(addr);

				// seed the random generator, this is a bit ugly that its done here
				// but hey, it works!
				srand((unsigned int)time(NULL)); 

				// Create a UDP socket.
				if ((info->sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
				{
								perror("Fatal error! Could not create a UDP socket!");
								return(-1);
								// Här finns det mer att göra! Man kan ta reda på vad som 
								// gick fel. TODO
				}

				// Set up remote address.
				info->server_addr.sin_family = AF_INET;		// IPv4
				info->server_addr.sin_port = htons(port); // Port, to big endian.
				info->server_addr.sin_addr = *((struct in_addr *)info->host->h_addr_list[0]); //address
				bzero(&(info->server_addr.sin_zero), 8); /* padding */

				// For debugging only, so we can print the IP.
				// This function converts 0011..0010 to 127.0.0.1
				inet_ntop(AF_INET, &(info->server_addr.sin_addr), pip, 256);

				dp(">>>>>>> Some debugging output <<<<<<<\n"); 
				dp("\tinfo->sock: %i\n", info->sock);
				dp("\tinfo->server_addr.sin_port: %i\n", 
												ntohs(info->server_addr.sin_port));
				dp("\tinfo->server_addr.sin_addr: %s\n", pip);

				return 1;
}

/*
 * Sends data in a reliable fashion!
 * Params: pu_handle - the socket to use
 * 					    data - the data to send // TODO max size is ~65KB, do check!
 * 					     len - size of data
 * Return: TODO. Bytes sent would be nice.
 * 				 -1 for errors.
 */
int pu_send(const struct pu_info *pu_handle, const char * const data, size_t data_size)
{
				int packets_to_send; // Number of packets to send.
				short *status_vector; // Status (ACK? for each packet).
				int *status_vector_ids; // Status (ACK? for each packet).
				pthread_t **sender_threads;

				// packets to send, round up
				packets_to_send = (data_size-1)/PU_MAX_PACKET_SIZE+1;
				if (packets_to_send == 0)
								packets_to_send = 1;

				// For each packet we will have a send/resending thread. We have to save
				// a handle to each thread so we know when they are done.
				sender_threads = (pthread_t**)malloc(packets_to_send * sizeof(struct pthread_t*));

				status_vector_ids = (int *)malloc(packets_to_send * sizeof(int));
				// Better put this on the heap. 
				status_vector = (short *)malloc(packets_to_send * sizeof(short));
				if(status_vector == NULL)
				{
								printf("Fatal error! Most likely ran out of memory!\n");
								return(-1);
				}
				else 
				{				
								// Make sure they are all set to zero.
								int i;
								for(i = 0; i < packets_to_send; i++)
												status_vector[i] = 0;
				}

				// Set up the ACK-handler and start it.
				pthread_t ack_thread_handle;
				struct pu_ack_handler_info *ack_handler_info;
				ack_handler_info = (struct pu_ack_handler_info *)malloc(sizeof(struct pu_ack_handler_info));

				// Mutex
				pthread_mutex_t *ack_mutex_handle;
				ack_mutex_handle = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
				pthread_mutex_init(ack_mutex_handle, NULL);

				ack_handler_info->receiver = (struct sockaddr*)&(pu_handle->server_addr);
				ack_handler_info->ack_mutex_handle = ack_mutex_handle;
				ack_handler_info->sock = pu_handle->sock;
				ack_handler_info->status_vector_ids = status_vector_ids;
				ack_handler_info->status_vector = status_vector;
				ack_handler_info->status_vector_length = packets_to_send;
				pthread_create(&ack_thread_handle, NULL, ack_handler, (void *)ack_handler_info);
	
				printf("pu_send debug information:\n");
				printf("\tdata size: %i\n", (int)data_size);
				printf("\tmax packet size: %i\n", PU_MAX_PACKET_SIZE);
				printf("\tpackets to send: %i\n", packets_to_send);

				// Do something with each packet. 
				// That is, send them and create a thread that listens for acks.
				int first_id, offset, i;
				offset = 0;

				first_id = rand();
				for(i = 0; i < packets_to_send; i++)
				{
								struct pu_packet *packet = (struct pu_packet *)malloc(sizeof(struct pu_packet));
								packet->packet_id = first_id++;
								status_vector_ids[i] = packet->packet_id;

								// if the data wont fit into one UDP packet
								// we'll split it up into chunks
								if (data_size > PU_MAX_PACKET_SIZE)
								{
												packet->total_size = PU_MAX_PACKET_SIZE + sizeof(int);
												data_size -= PU_MAX_PACKET_SIZE;
								} 
								else 
								{
												packet->total_size = data_size + sizeof(int);
								}

								// Now lets create a memory area with the id followed by the
								// data so we can send it all in one fat UDP packet.
								//
								// We need to store an int + the data.
							  int packet_size = sizeof(int)+sizeof(char)*packet->total_size;
								char *packet_data = (char *)malloc(packet_size);

								// copy int then data
								dp("creating memory storage. reading data from: %p\n", data+offset);

								memcpy(packet_data, &packet->packet_id, sizeof(int));
								packet_data += sizeof(int);
								memcpy(packet_data, data+offset, packet_size - sizeof(int));
								packet_data -= sizeof(int);
								packet->data = packet_data;

								// update offset AFTER new data has been copied
								offset += PU_MAX_PACKET_SIZE;

								dp("pu_send(): Sending packet #%i of %i (id %i [%x]) packet size: %i pointer address: %p, packet->data: %p\n", i+1, 
																					packets_to_send, 
																					packet->packet_id,
																					packet->packet_id,
																					packet->total_size,
																					packet_data,
																					packet->data);
								

								// Create a new thread thats responsible for resending the data, if no ACKs
								// has been received.
								struct pu_resend_handler_info *resend_handler_info;
								resend_handler_info = (struct pu_resend_handler_info *)malloc(sizeof(struct pu_resend_handler_info));
								resend_handler_info->sock = pu_handle->sock;
								resend_handler_info->status_vector = status_vector;
								resend_handler_info->status_vector_ids = status_vector_ids;
								resend_handler_info->status_vector_length	= packets_to_send;
								resend_handler_info->packet_info = packet;
								resend_handler_info->ack_mutex_handle = ack_mutex_handle;
								resend_handler_info->receiver = (struct sockaddr *)&(pu_handle->server_addr);

								pthread_t *resend_thread_handle = (pthread_t *)malloc(sizeof(pthread_t)); 
								sender_threads[i] = resend_thread_handle;
								pthread_create(resend_thread_handle, NULL, resend_handler, resend_handler_info);
				}

				// Make sure to wait so all the packages has been acknowledged.
				dp("pu_send() has reached its end and is now waiting.\n");
				pthread_join(ack_thread_handle, NULL);

				// Dont forget all the sender threads!
				int j;
				for(j = 0; j < packets_to_send; j++)
				{
							
								dp("pu_send(): Waiting for thread %i of %i\n", j+1, packets_to_send);
								pthread_join(*(sender_threads[j]), NULL);	
								dp("pu_send():Done with thread %i of %i\n\n", j+1, packets_to_send);
				}
				
				free(sender_threads);
				free(status_vector);
				free(status_vector_ids);
				free(ack_mutex_handle);

				dp("pu_send(): done waiting, returning.\n");
				return 1;
}

/**
 * This function is responsible for resnding packages.
 * It looks at the status_vector when the timeout has been
 * reached. If the status_vector hasnt been checked of, the package is resent
 */
void *resend_handler(void *arg) 
{
				struct pu_resend_handler_info *info = (struct pu_resend_handler_info *)arg;
				pthread_mutex_t *ack_mutex_handle = info->ack_mutex_handle;

				dp("send_handler(): Sending a packet.\n");
				dp("\tpacket_info pointer:: %p\n", (void *)info->packet_info);
				dp("\tpacket id: %i\n", info->packet_info->packet_id);
				dp("\tpacket size: %i\n", info->packet_info->total_size);
				dp("\tpacket start location: %p\n", info->packet_info->data);

				// send
				int return_code;
send:
				return_code	= 0;
				return_code = sendto(info->sock, // socket to use
							 info->packet_info->data,
							 info->packet_info->total_size,
							 0, // flags
							 info->receiver, // remote address
							 sizeof(*(info->receiver)) // size of address struct
				);

				if (return_code == -1) 
				{
								dp("resend_handler(): sendto returned with an error.\n");
								printf("\t%s\n", strerror(errno));
								return NULL;
				}

				struct timespec sleep_time;

				// Sleep this thread for random amount of time in the interval 
				// PU_RETRANSMISSION_TIME to PU_RETRANSMISSION_TIME + PU_RETRANSMISSION_TIME/10.
				// This is to prevent correlation in time when resending packages.
				//sleep(PU_RETRANSMISSION_TIME);
				sleep_time.tv_sec = 0;
				sleep_time.tv_nsec = 10000000;
				
				int ret = nanosleep(&sleep_time, NULL);
				if (ret == -1)
				{
								printf("nanosleep error: %s\n",strerror(errno));
				}
				sleep_time.tv_nsec = rand() % 100000000;

				ret = nanosleep(&sleep_time, NULL);
				if (ret == -1)
				{
								printf("nanosleep error: %s\n",strerror(errno));
				}
			

				dp("resend_handler(): Thread just woke up, lets check if an ACK has been received.\n");

				// CRITICAL SECTION
				pthread_mutex_lock(ack_mutex_handle);
				int i;
				for(i = 0; i < info->status_vector_length; i++) 
				{
								if (info->status_vector_ids[i] == info->packet_info->packet_id) 
								{
												dp("\tfound our index for packet, it was %i\n", i);
												dp("\tinfo->status_vector[i]: %i\n", info->status_vector[i]);

												if(info->status_vector[i] == PU_ACK)
												{
																dp("\tPacket has been acknowledged. closing thread.\n");

																// ALTERNATIVE END OF CRITICAL SECTION
																pthread_mutex_unlock(ack_mutex_handle);
																return arg;
												}
												else
												{
																dp("\tPacket has NOT been acknowledged. Resending!\n");
																// resend indefinetly

																// ALTERNATIVE END OF CRITICAL SECTION
																pthread_mutex_unlock(ack_mutex_handle);
																goto send;
												}
								}
				}

				// END OF CRITICAL SECTION
				pthread_mutex_unlock(ack_mutex_handle);

				free(arg);
				return arg;
}

/**
 * This function will be run in a parallel thread.
 * It will receive acks and mark them off in the
 * status_vector.
 */
void *ack_handler(void *arg) 
{
				struct pu_ack_handler_info *puck = (struct pu_ack_handler_info*)arg;
				pthread_mutex_t *ack_mutex_handle = puck->ack_mutex_handle;
				int i, sum;

				int id_from_socket;
				socklen_t addr_len = sizeof(struct sockaddr);

receive:
				dp("ack_handler: Waiting on recvfrom\n");
				int bytes_read = recvfrom(puck->sock, &id_from_socket, 4, 0, puck->receiver, &addr_len);
				dp("ack_handler: read %i bytes, value: %i [%x]\n", bytes_read, id_from_socket, id_from_socket);

				// find correct id
				// START OF CRITICAL SECTION
				pthread_mutex_lock(ack_mutex_handle);
				for(i = 0; i < puck->status_vector_length; i++) 
				{
								if (puck->status_vector_ids[i] == id_from_socket)
								{
												// if found, mark packet as PU_ACK
												dp("\t found status vector index for packet id %i\n", id_from_socket);
												dp("\t marking it as ACKed!"); 
												puck->status_vector[i] = PU_ACK;
								}
				}
				dp("\n");

				// any packets not acked?
				int acked = 0;
				for(sum = 0, i = 0; i < puck->status_vector_length; i++) 
				{
								dp("[%i:%i] ", i, puck->status_vector[i]);

								if (puck->status_vector[i] == PU_ACK)
												acked++;

								// sum the status
								sum+= puck->status_vector[i];
				}
				dp("Number of ACKed packages %i out of %i packages total.\n", acked, puck->status_vector_length);

				// If sum is -2*status_vector_length then all have been acked.
				if(sum == -2*puck->status_vector_length)
				{
								printf("ack_handler(): sum of all elements in status_vector was -2 * %i. this means all packets have been ACKed, terminating thread.\n", puck->status_vector_length);

								// ALTERNATIVE END OF CRITICAL SECTION
								pthread_mutex_unlock(ack_mutex_handle);
								goto done;
				}
				// END OF CRITICAL SECTION
				pthread_mutex_unlock(ack_mutex_handle);

				// read next package
				goto receive;

done:
				// Important! This one is easy to forget.
				free(arg);
	
				dp("ack_handler(): returns\n");
				return arg;
}
