#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

/* Constansts */
const int PORT_NUM = 6666;
const int BUFF_LEN = 1024;

int main(int argc, char *argv[]) 
{
				if (argc != 2)
				{
								printf("\nUsage: ./client <server address>\n");
								fflush(stdout);
								exit(0);
				}
				else
				{
								printf("\nConnecting to %s.\n", argv[1]);
								fflush(stdout);
				}

				int sock;
				struct sockaddr_in server_addr;
				struct hostent *host;
				char send_data[BUFF_LEN];

				host = (struct hostent *) gethostbyname(argv[1]);

				if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
				{
								perror("Socket error!");
								exit(1);
				}

				server_addr.sin_family = AF_INET;
				server_addr.sin_port = htons(PORT_NUM);
				server_addr.sin_addr = *((struct in_addr *)host->h_addr);
				bzero(&(server_addr.sin_zero), 8);

				while (1) 
				{
								printf("Type something you want to send to the server (q or Q to quit):");
								gets(send_data);

								if ((strcmp(send_data, "q") == 0) || strcmp(send_data, "Q") == 0)
												break;
								else
												sendto(sock, send_data, strlen(send_data), 0,
																(struct sockaddr *)&server_addr, sizeof(struct sockaddr));
				}

				return 0;
}
