/* Simple UDP echo server */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

/* Constansts */
const int PORT_NUM = 6666;
const int BUFF_LEN = 2050;

int main() 
{
				int sock;
				int addr_len, bytes_read;
				char recv_data[BUFF_LEN];

        FILE *fp;

        if((fp=fopen("test.txt", "w"))==NULL) {
          printf("Cannot open file.\n");
          exit(1);
        }

        fprintf(fp, "this is a test %d %f", 10, 20.01);

				/* sockaddr_in ser ur sahar.
				struct sockaddr_in {
								short int          sin_family;  // Adressfamilj
								unsigned short int sin_port;    // Port (Network byte order!)
								struct in_addr     sin_addr;    // ipadress, se nedan for definition.
								unsigned char      sin_zero[8]; // Padda till samma storlek som sockaddr
				};		 
				*/

				/* in_addr ser ut sahar.
				struct in_addr {
								unsigned long s_addr; // IP-adress, 32-bitar (4 bytes) (Network byte order!) 
				};
				*/
				struct sockaddr_in server_addr, client_addr;

				if((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
				{
								perror("Socket error!");
								exit(1);
				}	

				server_addr.sin_family = AF_INET;
				server_addr.sin_port = htons(PORT_NUM);
				server_addr.sin_addr.s_addr = INADDR_ANY;
				bzero(&(server_addr.sin_zero), 8);

				if(bind(sock, (struct sockaddr *)&server_addr,
								sizeof(struct sockaddr)) == -1)
				{
								perror("Bind error!");
								exit(1);
				}

				/* Size of sockaddr */
				addr_len = sizeof(struct sockaddr);

				printf("UDPServer waiting for client on port %i.\n", PORT_NUM);
				fflush(stdout);

				while(1)
				{
								bytes_read = recvfrom(sock, recv_data, BUFF_LEN-1, 0,
																(struct sockaddr *)&client_addr, &addr_len);
								recv_data[bytes_read] = '\0';


								// TODO
								int res = sendto(sock, recv_data, 4, 0, (struct sockaddr *)&client_addr, (size_t)addr_len);
								if (res == -1) 
								{
												printf("Error, couldnt respond!");
												perror(errno);
												exit(1);
								}

								char client_str[32];
								inet_ntop(AF_INET, &client_addr.sin_addr, client_str, sizeof(client_str));

								printf("\n(%s:%d) said : ", client_str, ntohs(client_addr.sin_port));
								printf("%s", recv_data);
								fflush(stdout);
				}

				return 0;
}
